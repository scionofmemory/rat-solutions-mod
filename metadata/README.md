# Rat Solutions

Shows the solution to the maze in the Resourceful Rat's Lair via arrows on the ground. Also automatically removes the dust & debris from the trapdoor in the mines.

There are 3 additional commands available:

* ratsolutions enable - Enables the mod's effects,
* ratsolutions disable - Disables the mod's effects,
* ratsolutions show - Prints the solution path out of maze to the console.

## Special Thanks
* Dallan for making the icon.

---

## Changelog

* 1.0.0 - Initial BepInEx port