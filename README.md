# Rat Solutions Mod

A quality of life mod for Enter the Gungeon that shows the path through the rat maze.

## Prerequisites

Requires Visual Studio 2022 with .NET 6 (for Cake build system) and .NET 3.5 installed via Windows.

## Building and packaging

To build and create release zip:

```powershell
.\build.ps1 --target=Package
```

The output will be in `publish\RatSolutionsMod_{version}.zip`.

## Project Structure

  - .config  - dotnet local tool configuration
  - metadata - required metadata files for thunderstore mods
  - src      - source code for mod
