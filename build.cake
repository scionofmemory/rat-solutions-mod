#addin nuget:?package=Cake.Json&version=7.0.1
#addin nuget:?package=Newtonsoft.Json&version=11.0.2

#tool nuget:?package=NUnit.ConsoleRunner&version=3.11.1

//////////////////////////////////////////////////////////////////////
// ARGUMENTS
//////////////////////////////////////////////////////////////////////

var target = Argument("target", "Default");
var configuration = Argument("configuration", "Release");

//////////////////////////////////////////////////////////////////////
// PREPARATION
//////////////////////////////////////////////////////////////////////

var solutionFile = "./RatSolutions.sln";

//////////////////////////////////////////////////////////////////////
// TASKS
//////////////////////////////////////////////////////////////////////

Task("Restore-NuGet-Packages")
    .Does(() =>
{
    NuGetRestore(solutionFile);
});

Task("Build")
    .IsDependentOn("Restore-NuGet-Packages")
    .Does(() =>
{
    var settings = new DotNetBuildSettings
    {
        Configuration = configuration
    };

    DotNetBuild(solutionFile, settings);
});

Task("Create-Zip-Files")
    .IsDependentOn("Build")
    .Does(() => 
{
    CleanDirectories("./publish/**");
    var innerPublishDir = Directory("./publish/ratsolutions");
    CreateDirectory(innerPublishDir);

    CopyFiles("./metadata/*", innerPublishDir);

    var outDir = Directory($"./src/RatSolutionsMod/bin/{configuration}/net35");
    CopyFileToDirectory(outDir + File("RatSolutionsMod.dll"), innerPublishDir);
    // pdbs don't seem to work
    // CopyFileToDirectory(outDir + File("RatSolutionsMod.pdb"), innerPublishDir);

    var manifest = File("./metadata/manifest.json");

    var manifestJson = ParseJsonFromFile(manifest);
    string version = manifestJson["version_number"].ToString();

    var zipFile = File($"./publish/RatSolutionsMod_{version}.zip");
    Information($"Creating zip for version:{version}, Zip:{zipFile}");

    Zip(innerPublishDir, zipFile);
});

Task("Package")
    .IsDependentOn("Create-Zip-Files");

//////////////////////////////////////////////////////////////////////
// TASK TARGETS
//////////////////////////////////////////////////////////////////////

Task("Default")
    .IsDependentOn("Build");

//////////////////////////////////////////////////////////////////////
// EXECUTION
//////////////////////////////////////////////////////////////////////

RunTarget(target);
