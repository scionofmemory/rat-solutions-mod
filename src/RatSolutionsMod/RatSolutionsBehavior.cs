﻿using System;
using System.Collections;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using Dungeonator;
using UnityEngine;

namespace RatSolutionsMod
{
    public class RatSolutionsBehavior : MonoBehaviour
    {
        private const int SenseOfDirectionId = 209;

        private static Func<ResourcefulRatMazeSystemController, bool> getMazeIsActive = CreateFieldAccessor<ResourcefulRatMazeSystemController, bool>("m_mazeIsActive");

        private static Func<ResourcefulRatMazeSystemController, bool> getHasLeftEntrance = CreateFieldAccessor<ResourcefulRatMazeSystemController, bool>("m_playerHasLeftEntrance");

        private static Func<ResourcefulRatMazeSystemController, int> getCurrentTargetDirectionIndex = CreateFieldAccessor<ResourcefulRatMazeSystemController, int>("m_currentTargetDirectionIndex");

        public bool Enabled = true;

        private DungeonData.Direction[] ratSolution;

        private ResourcefulRatMazeSystemController controller;

        private GameObject arrowVfx;

        private bool coroutineRunning = false;

        private void Start()
        {
            var senseOfDirection = PickupObjectDatabase.GetById(SenseOfDirectionId) as SenseOfDirectionItem;
            if (!senseOfDirection)
            {
                StaticLogger.LogInfo("Did not find sense of direction item!");
                return;
            }

            arrowVfx = senseOfDirection.arrowVFX;
            GameManager.Instance.OnNewLevelFullyLoaded += HandleOnNewLevelFullyLoaded;
        }

        private void Update()
        {
            if (!Enabled)
            {
                return;
            }

            if (GameManager.Instance.CurrentLevelOverrideState != GameManager.LevelOverrideState.RESOURCEFUL_RAT || GameManager.Instance.IsLoadingLevel)
            {
                return;
            }

            if (ratSolution == null)
            {
                ratSolution = GameManager.GetResourcefulRatSolution();
                var sb = new StringBuilder();
                foreach (var dir in ratSolution)
                {
                    sb.Append(dir).Append(", ");
                }

                sb.Length -= 2;
                if (arrowVfx)
                {
                    StaticLogger.LogDebug($"Directions: {sb}");
                }
                else
                {
                    StaticLogger.LogInfo($"Directions: {sb}");
                }
            }

            if (!controller)
            {
                for (int i = 0; i < GameManager.Instance.Dungeon.data.rooms.Count; i++)
                {
                    var roomHandler = GameManager.Instance.Dungeon.data.rooms[i];
                    var components = roomHandler.GetComponentsAbsoluteInRoom<ResourcefulRatMazeSystemController>();
                    if (components.Count > 0)
                    {
                        controller = components[0];
                        controller.Initialize();
                    }
                }
            }

            if (controller)
            {
                bool mazeIsActive = getMazeIsActive(controller);
                if (!mazeIsActive)
                    return;

                bool hasLeftEntrance = getHasLeftEntrance(controller);
                if (!hasLeftEntrance)
                    return;
            }
            else
            {
                return;
            }

            if (coroutineRunning)
                return;

            var player = GameManager.Instance.BestActivePlayer;
            if (player.IsInCombat)
                return;

            int index = getCurrentTargetDirectionIndex(controller);
            if (index < 0 || index >= ratSolution.Length)
                return;

            var direction = ratSolution[index];
            
            if (arrowVfx)
            {
                StaticLogger.LogDebug($"Next direction is {direction}");
                coroutineRunning = true;
                StartCoroutine(ShowDirection(player.CurrentRoom, direction));
            }
        }

        private IEnumerator ShowDirection(RoomHandler room, DungeonData.Direction direction)
        {
            try
            {
                var center = room.GetCenterCell().ToCenterVector2();
                if (!TryGetDirectionToVector2(direction, out var directionVector))
                    yield break;

                var directionAngle = BraveMathCollege.Atan2Degrees(directionVector);
                yield return new WaitForSeconds(0.5f);
                while (GameManager.Instance?.BestActivePlayer?.CurrentRoom == room)
                {
                    var position = center;
                    for (int i = 0; i < 3; i++)
                    {
                        SpawnManager.SpawnVFX(arrowVfx, position, Quaternion.Euler(0f, 0f, directionAngle), false);
                        yield return new WaitForSeconds(0.5f);
                        position += directionVector * 3;
                    }

                    yield return new WaitForSeconds(3);
                }
            }
            finally
            {
                coroutineRunning = false;
            }
        }

        private void HandleOnNewLevelFullyLoaded()
        {
            if (!Enabled)
            {
                return;
            }

            int count = 0;
            for (int j = 0; j < StaticReferenceManager.AllRatTrapdoors.Count; j++)
            {
                var trapDoor = StaticReferenceManager.AllRatTrapdoors[j];
                if (trapDoor)
                {
                    trapDoor.RevealPercentage = 1f;
                    trapDoor.BlendMaterial.SetFloat("_BlendMin", trapDoor.RevealPercentage);
                    trapDoor.LockBlendMaterial.SetFloat("_BlendMin", trapDoor.RevealPercentage);
                    count++;
                }
            }

            StaticLogger.LogDebug("Revealed {0} trap doors", count);
        }

        private static bool TryGetDirectionToVector2(DungeonData.Direction direction, out Vector2 vecDir)
        {
            switch (direction)
            {
                case DungeonData.Direction.NORTH:
                    vecDir = Vector2.up;
                    return true;
                case DungeonData.Direction.EAST:
                    vecDir = Vector2.right;
                    return true;
                case DungeonData.Direction.SOUTH:
                    vecDir = Vector2.down;
                    return true;
                case DungeonData.Direction.WEST:
                    vecDir = Vector2.left;
                    return true;
                default:
                    StaticLogger.LogInfo($"Invalid direction: {direction}");
                    vecDir = Vector2.zero;
                    return false;
            }
        }

        private static Func<TInstance, TValue> CreateFieldAccessor<TInstance, TValue>(string fieldName)
        {
            var field = typeof(TInstance).GetField(fieldName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            if (field == null)
            {
                StaticLogger.LogWarning($"Field with name '{fieldName}' not found on type {typeof(TInstance)}");
                return i => default;
            }

            var dynamicMethod = new DynamicMethod($"get_{fieldName}_dynamicaccessor", typeof(TValue), new Type[] { typeof(TInstance) }, typeof(TInstance), true);
            var ilGen = dynamicMethod.GetILGenerator();
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Ldfld, field);
            ilGen.Emit(OpCodes.Ret);
            return (Func<TInstance, TValue>)dynamicMethod.CreateDelegate(typeof(Func<TInstance, TValue>));
        }
    }
}
