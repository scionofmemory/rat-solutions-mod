﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BepInEx.Logging;

namespace RatSolutionsMod
{
    internal static class StaticLogger
    {
        public static ManualLogSource Source;

        public static void LogError(string message) => Source?.LogError(message);

        public static void LogWarning(string message) => Source?.LogWarning(message);

        public static void LogInfo(string message) => Source?.LogInfo(message);

        public static void LogDebug(string message) => Source?.LogDebug(message);

        public static void LogDebug<T>(string format, T arg) => LogDebug(string.Format(format, arg));
    }
}
