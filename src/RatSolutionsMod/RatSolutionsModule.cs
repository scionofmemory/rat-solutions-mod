﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BepInEx;

namespace RatSolutionsMod
{
    [BepInDependency(ETGModMainBehaviour.GUID)]
    [BepInPlugin(GUID, NAME, VERSION)]
    public class RatSolutionsModule : BaseUnityPlugin
    {
        public const string GUID = "glorfindel.etg.ratsolutions";
        public const string NAME = "RatSolutions";
        public const string VERSION = "1.0.0";

        public RatSolutionsBehavior behavior;

        public void Start()
        {
            StaticLogger.Source = Logger;
            ETGModMainBehaviour.WaitForGameManagerStart(GmStart);
        }

        public void GmStart(GameManager gm)
        {
            try
            {
                behavior = ETGModMainBehaviour.Instance.gameObject.AddComponent<RatSolutionsBehavior>();
                ETGModConsole.Commands.AddGroup("ratsolutions");
                var group = ETGModConsole.Commands.GetGroup("ratsolutions");
                group.AddUnit("enable", args =>
                {
                    behavior.Enabled = true;
                    ETGModConsole.Log("RatSolutions enabled");
                });

                group.AddUnit("disable", args =>
                {
                    behavior.Enabled = false;
                    ETGModConsole.Log("RatSolutions disabled");
                });

                group.AddUnit("show", args =>
                {
                    var solution = GameManager.GetResourcefulRatSolution();
                    var sb = new StringBuilder();
                    foreach (var dir in solution)
                    {
                        sb.Append(dir).Append(", ");
                    }

                    sb.Length -= 2;
                    ETGModConsole.Log($"Directions: {sb}");
                });
            }
            catch (Exception e)
            {
                StaticLogger.LogError($"RatSolutions v{VERSION} Error: {e}");
            }
        }
    }
}
